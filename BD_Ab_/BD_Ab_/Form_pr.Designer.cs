﻿namespace BD_Ab_
{
    partial class Form_pr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_ok = new System.Windows.Forms.Button();
            this.textBox_name_pr = new System.Windows.Forms.TextBox();
            this.textBox_score = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_сlose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_ok
            // 
            this.button_ok.Location = new System.Drawing.Point(35, 141);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(110, 42);
            this.button_ok.TabIndex = 0;
            this.button_ok.Text = "ОК";
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // textBox_name_pr
            // 
            this.textBox_name_pr.Location = new System.Drawing.Point(153, 38);
            this.textBox_name_pr.Name = "textBox_name_pr";
            this.textBox_name_pr.Size = new System.Drawing.Size(170, 22);
            this.textBox_name_pr.TabIndex = 1;
            // 
            // textBox_score
            // 
            this.textBox_score.Location = new System.Drawing.Point(153, 99);
            this.textBox_score.Name = "textBox_score";
            this.textBox_score.Size = new System.Drawing.Size(170, 22);
            this.textBox_score.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Название компании";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Рейтинг";
            // 
            // button_сlose
            // 
            this.button_сlose.Location = new System.Drawing.Point(178, 141);
            this.button_сlose.Name = "button_сlose";
            this.button_сlose.Size = new System.Drawing.Size(110, 42);
            this.button_сlose.TabIndex = 5;
            this.button_сlose.Text = "Отмена";
            this.button_сlose.UseVisualStyleBackColor = true;
            this.button_сlose.Click += new System.EventHandler(this.button_сlose_Click);
            // 
            // Form_pr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 201);
            this.Controls.Add(this.button_сlose);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_score);
            this.Controls.Add(this.textBox_name_pr);
            this.Controls.Add(this.button_ok);
            this.Name = "Form_pr";
            this.Text = "Form_pr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_сlose;
        public System.Windows.Forms.TextBox textBox_name_pr;
        public System.Windows.Forms.TextBox textBox_score;
    }
}