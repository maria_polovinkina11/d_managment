﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BD_Ab_
{
    public partial class Form_cont : Form
    {
        public Form_cont()
        {
            InitializeComponent();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> ProviderData
        {
            set
            {
                comboBox_provider.DataSource = value.ToArray();
                comboBox_provider.DisplayMember = "value";
            }
        }

        public List<string> TypeData
        {
            set
            {
                comboBox_type.DataSource = value.ToArray();
                comboBox_type.DisplayMember = "value";
            }
        }



        public int ProviderId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_provider.SelectedItem).Key;
            }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_provider.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                comboBox_provider.SelectedIndex = idx;
            }
        }

        public int TypeId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_provider.SelectedItem).Key;
            }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_provider.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    idx++;
                }
                comboBox_provider.SelectedIndex = idx;
            }
        }



    }
}
